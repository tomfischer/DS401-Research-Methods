# DS401 Research Methods

## Introduction

On this site, I share materials related to the course DS401 Research Methods, which I teach in the Fall semester of 2023 at the [School of Design](http://designschool.sustech.edu.cn) at [Southern University of Science and Technology](https://www.sustech.edu.cn) in Shenzhen. Material accumulates as the semester progresses, week by week.

SD401 Research Methods is aligned with the concurrent course DS402 Research Project. It introduces students to methods, models, and theories of design and design research and related visualization and academic writing techniques. It guides students in selecting and applying these to support individual design projects conducted in parallel with SD402. Through lectures, readings, and guided discussions, students will establish repertoires of methods and approaches to conduct well-documented and appropriately reasoned design research inquiries.

## Week 01: Knowledge

[11-Sep-2023 Slide Deck](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/slide-decks/DS401_Design_Research_01_Welcome_Knowledge.pdf)

![DS401 Week 01 Slide 20](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/preview_images/DS401_WK01_preview01.jpg)

Key take-away (Slide 20): _There are different kinds of knowing, coming to us through different kinds of activity and from different sources._

## Week 02: Scientific Research and Design Research

[18-Sep-2023 Slide Deck](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/slide-decks/DS401_Design_Research_02_Scientific_Research_Design_Research.pdf)

![DS401 Week 02 Slide 22](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/preview_images/DS401_WK02_preview01.jpg)

Key take-away (Slide 22): _Between Scientific Research and Design Research, we frequently encounter dilemmas between rigour (doing things right) and relevance (doing the right thing)._

## Week 03: Design Process Models I

[25-Sep-2023 Slide Deck](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/slide-decks/DS401_Design_Research_03_Design_Process_Models_I.pdf)

![DS401 Week 03 Slide 04](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/preview_images/DS401_WK03_preview01.jpg)

Key take-away (Slide 04): _Various kinds of design process models exist. They may be categorized into formal/informal, linear/circular, and descriptive/prescriptive design process models._

## Week 04: Design Process Models II

[09-Oct-2023 Slide Deck](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/slide-decks/DS401_Design_Research_04_Design_Process_Models_II.pdf)

![DS401 Week 04 Slide 14](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/preview_images/DS401_WK04_preview01.jpg)

Key take-away (Slide 14): _Different perspectives on the design process tend to favor different design process models. The external perspective of design managers tends towards describing the design process in linear terms while the subjective perspective of design practitioners tends towards depicting the design process in circular terms._

## Week 05: Rise and Fall of the Design Methods Movement; Hypotheses and Research Questions

[16-Oct-2023 Slide Deck](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/slide-decks/DS401_Design_Research_05_Design_Methods_Research_Questions.pdf)

![DS401 Week 05 Slide 20](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/preview_images/DS401_WK05_preview01.jpg)

Key take-away (Slide 20): _The Design Methods Movement is over. Methods continue to play a role in some areas of design: for gathering insights about users, context, precedent, for creative divergence, and methods determined by designers within the contexts of their application. These usually do not claim scientific rigor. Formal methods may also be deployed in formal work within the context of broader design projects._

## Week 06: Design Research Methods

[23-Oct-2023 Slide Deck](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/slide-decks/DS401_Design_Research_06_Design_Research_Methods.pdf)

![DS401 Week 06 Slide 29](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/preview_images/DS401_WK06_preview01.jpg)

Key take-away (Slide 29): _Straight-forward design research methods such as prototyping and testing can be significantly more demanding, rigorous, reliable, and genuinely open to failure than some seemingly elaborate, diagram-based design research methods/formats._

## Week 07: Interim Presentations

## Week 08: Research Ethics and Working With Literature

[23-Oct-2023 Slide Deck](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/slide-decks/DS401_Design_Research_07_Research_Ethics_Working_With_Literature.pdf)

![DS401 Week 08 Slide 6](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/preview_images/DS401_WK08_preview01.jpg)

Key take-away (Slide 06): _The purpose of research ethics and its related procedures is to safeguard the welfare of living systems affected by research activities._

## Week 09: Introduction of FYP Overview Table


## Week 10: Testing and Evaluation

[20-Nov-2023 Slide Deck](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/slide-decks/DS401_Design_Research_10_Testing_and_Evaluation.pdf)

![DS401 Week 10 Slide 15](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/preview_images/DS401_WK10_preview01.jpg)

Key take-away (Slide 15): _Consider design reviews (crits) not as mere assessment chores, but as expert feedback sessions, which you may document and analyze as a part of your projects' testing and exaluation._


## Week 11: Design Research Case Studies

[27-Nov-2023 Slide Deck](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/slide-decks/DS401_Design_Research_11_Design_Research_Case_Studies.pdf)

![DS401 Week 11 Slide 05](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/preview_images/DS401_WK11_preview01.jpg)

Key take-away (Slide 05): _._


## Week 13: Design Research Case Studies

[11-Dec-2023 Slide Deck](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/slide-decks/DS401_Design_Research_12_Final_Presentation_Briefing.pdf)

![DS401 Week 13 Slide 04](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/preview_images/DS401_WK13_preview01.jpg)

Key take-away (Slide 04): _._

## Week 16: Summark

[02-Jan-2024 Slide Deck](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/slide-decks/DS103_Designing_for_Beginners_16_Summary.pdf)

![DS401 Week 16 Slide 20](https://gitlab.com/tomfischer/DS401-Research-Methods/-/raw/main/preview_images/DS401_WK16_preview01.jpg)

Key take-away (Slide 20): _The exam will consist mainly of text questions with some multiple-choice questions and questions that ask for drawings and comparisons of process model diagrams._
